# NPM Sass Docker container

Using [Node on Docker Hub](https://hub.docker.com/_/node/) as a parent this
provides a simple node container that also has the ability to do sass. We use
it mostly to gulp combine our CSS and JS scripts for our projects. It is
important to note that we only generally use LTS versions of Node unless
necessary

## Building

    docker build --force-rm -f node-6-v1.0.1/Dockerfile -t bbpdev/npm-sass:6-v1.0.1 .
    docker build --force-rm -f node-8-v1.0.1/Dockerfile -t bbpdev/npm-sass:8-v1.0.1 .

## Versions

* Node 6.12.0 with Sass 3.5.3
* Node 8.9.1 with Sass 3.5.3

## Pushing

    docker login
    docker push bbpdev/npm-sass:6-v1.0.1
    docker push bbpdev/npm-sass:8-v1.0.1

## Useful information

As advised in the [Docker Hub image](https://hub.docker.com/_/node/) if your considering to mount a volume to this container then do so to `/usr/src/app`

e.g

    node:
      image: bbpdev/npm-sass:6-v1.0.0
      volumes:
        - ../src/:/usr/src/app
      working_dir: /usr/src/app

or

    $ docker run -it --rm --name my-running-script -v "$PWD":/usr/src/app -w /usr/src/app bbpdev/npm-sass node your-daemon-or-script.js
